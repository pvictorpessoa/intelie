import unittest

from src.intelie import (
    convert_schema_to_dict, remove_old_one_to_one_relationship,
    add_line, remove_line, convert_facts
)


class TestIntelie(unittest.TestCase):

    def setUp(self):
        self.schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]
        self.resultado = []

    def convert_schema(self):
        return convert_schema_to_dict(self.schema)

    def test_convert_schema_to_dict(self):
        _schema_expected = {'endereço': 'one', 'telefone': 'many'}
        _schema = convert_schema_to_dict(self.schema)
        self.assertEqual(_schema, _schema_expected)

    def test_remove_old_one_to_one_relationship(self):
        resultado = []
        old_fact = ('gabriel', 'endereço', 'av rio branco, 109', True)
        resultado.append(old_fact)
        remove_old_one_to_one_relationship(
            self.convert_schema(),
            'gabriel',
            'endereço',
            resultado
            )
        self.assertEqual(resultado, [])

    def test_add_line(self):
        resultado = []
        values = ('gabriel', 'endereço', 'av rio branco, 109', True)

        add_line(resultado, *values)
        self.assertEqual(
            resultado,
            [
                ('gabriel', 'endereço', 'av rio branco, 109', True),
            ]
        )

    def test_remove_line(self):
        resultado = []
        values = ('gabriel', 'endereço', 'av rio branco, 109', True)
        resultado.append(values)
        remove_line(resultado, *values)
        self.assertEqual(resultado, [])

    def test_convert_facts(self):
        schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]

        facts = [
            ('gabriel', 'endereço', 'av rio branco, 109', True),
            ('joão', 'endereço', 'rua alice, 10', True),
            ('joão', 'endereço', 'rua bob, 88', True),
            ('joão', 'telefone', '234-5678', True),
            ('joão', 'telefone', '91234-5555', True),
            ('joão', 'telefone', '234-5678', False),
            ('gabriel', 'telefone', '98888-1111', True),
            ('gabriel', 'telefone', '56789-1010', True),
        ]
        expected_result = [
            ('gabriel', 'endereço', 'av rio branco, 109', True),
            ('joão', 'endereço', 'rua bob, 88', True),
            ('joão', 'telefone', '91234-5555', True),
            ('gabriel', 'telefone', '98888-1111', True),
            ('gabriel', 'telefone', '56789-1010', True)
        ]

        resultado = convert_facts(facts, schema)
        self.assertEqual(resultado, expected_result)

    def test_convert_empty_facts(self):
        schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]
        facts = []
        expected_result = []

        resultado = convert_facts(facts, schema)
        self.assertEqual(resultado, expected_result)

    def test_convert_one_fact(self):
        schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]
        facts = [
            ('paulo', 'endereço', 'av rio branco, 109', True),
        ]
        expected_result = [
            ('paulo', 'endereço', 'av rio branco, 109', True),
        ]

        resultado = convert_facts(facts, schema)
        self.assertEqual(resultado, expected_result)

    def test_convert_one_fact_removed(self):
        schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]
        facts = [
            ('paulo', 'telefone', '22 1234 5678', True),
            ('paulo', 'telefone', '22 1234 5678', False),
        ]
        expected_result = [
        ]

        resultado = convert_facts(facts, schema)
        self.assertEqual(resultado, expected_result)

    def test_convert_updated_cardinality_one(self):
        schema = [
            ('email', 'cardinality', 'one'),
        ]
        facts = [
            ('paulo', 'email', 'paulo@email.com', True),
            ('paulo', 'email', 'paulo@updated_email.com', True),
        ]
        expected_result = [
            ('paulo', 'email', 'paulo@updated_email.com', True),
        ]

        resultado = convert_facts(facts, schema)
        self.assertEqual(resultado, expected_result)

    def test_convert_updated_cardinality_many(self):
        schema = [
            ('telefone', 'cardinality', 'many'),
        ]
        facts = [
            ('paulo', 'telefone', '12345', True),
            ('paulo', 'telefone', '67890', True),
        ]
        expected_result = [
            ('paulo', 'telefone', '12345', True),
            ('paulo', 'telefone', '67890', True),
        ]

        resultado = convert_facts(facts, schema)
        self.assertEqual(resultado, expected_result)
