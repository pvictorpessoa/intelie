def convert_schema_to_dict(schema):
    _schema = {}
    for coluna in schema:
        _schema[coluna[0]] = coluna[2]
    return _schema


def remove_old_one_to_one_relationship(_schema, nome, coluna, resultado):
    if _schema.get(coluna) == 'one':
        _pessoas = list(
            filter(
                lambda x: x[0] == nome and x[1] == coluna, resultado
                )
            )

        if _pessoas:
            resultado.remove(_pessoas[0])


def add_line(resultado, *args):
    resultado.append(args)


def remove_line(resultado, *args):
    resultado.remove(args)


def perform_action(values, insert, resultado):
    add_line(resultado, *values) if insert else remove_line(resultado, *values)


def convert_facts(facts, schema):
    resultado = []
    _schema = convert_schema_to_dict(schema)
    for elem in facts:
        (nome, coluna, valor, insert) = elem
        remove_old_one_to_one_relationship(_schema, nome, coluna, resultado)
        values = nome, coluna, valor, True
        perform_action(values, insert, resultado)
    return resultado
